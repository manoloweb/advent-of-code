import os, re

input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2024-02-test.txt')

def is_safe(number_string):
    numbers = number_string.split(' ')
    direction = 1 if int(numbers[0]) < int(numbers[1]) else -1
    prev_number = int(numbers[0])
    for number in numbers[1:]:
        new_direction = 1 if prev_number < int(number) else -1 if prev_number > int(number) else 0
        if new_direction != direction:
            return False
        if abs(prev_number - int(number)) >3:
            return False
        prev_number = int(number)
    return True

for line in open(input_file_path):
    line_safe = is_safe(line.strip())
    print(line.strip(), line_safe)