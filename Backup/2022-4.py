import os

input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2022-4.txt')

a = range(2, 10)
b = range(1, 10)
c = range(5, 20)
d = range(5, 6)

def range_contain(ra, rb):
    outer = range(min(ra.start, rb.start), max(ra.stop, rb.stop))
    return 1 if outer == ra or outer == rb else 0

total_contain = 0

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        if line.strip() == '':
            continue
        ranges = line.strip().split(',')
        ra = ranges[0].split('-')
        rb = ranges[1].split('-')
        total_contain += range_contain(range(int(ra[0]), int(ra[1])), range(int(rb[0]), int(rb[1])))

print(total_contain)

# Second part

total_contain = 0

def range_intersect(ra, rb):
    return 1 if ra.start <= rb.stop and ra.stop >= rb.start else 0

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        if line.strip() == '':
            continue
        ranges = line.strip().split(',')
        ra = ranges[0].split('-')
        rb = ranges[1].split('-')
        total_contain += range_intersect(range(int(ra[0]), int(ra[1])), range(int(rb[0]), int(rb[1])))

print(total_contain)