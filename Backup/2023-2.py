import os, re
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-2.txt')

game_pattern = r'(\d+)\:'
color_pattern = r'(\d+) (\w+)'

max_count = {'red': 12, 'green': 13, 'blue': 14}

def check_max(color, count):
    if count > max_count[color]:
        return True
    return False

def validate_draw(draws):
    for draw in draws:
        draw = re.findall(color_pattern, draw.strip())
        for count, color in draw:
            if check_max(color, int(count)):
                return False
    return True

possible = 0

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        game = int(re.findall(game_pattern, line.strip())[0])
        draws = line.strip().split(':')[1].strip().split(';')
        if validate_draw(draws):
            possible += game
            
print(possible)

# Part 2

powers = 0

def max_colors(draws):
    colors = {'red': 0, 'green': 0, 'blue': 0}
    for draw in draws:
        draw = re.findall(color_pattern, draw.strip())
        for count, color in draw:
            colors[color] = int(count) if int(count) > colors[color] else colors[color]
    return colors

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        game = int(re.findall(game_pattern, line.strip())[0])
        draws = line.strip().split(':')[1].strip().split(';')
        max = max_colors(draws)
        powers += max['green'] * max['red'] * max['blue']

print(powers)