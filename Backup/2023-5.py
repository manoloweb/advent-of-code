import os, re
from itertools import count

input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-5.txt')

seeds = []
maps = {}
min_vals = []

with open(input_file_path, 'r') as input_file:
    doc = input_file.read().strip()
    parts = doc.split('\n\n')
    seeds = parts.pop(0).split(':')[1].strip().split(' ')
    seeds = list(map(int, seeds))

    for part in parts:
        map_name = part.split(':')[0].strip().split(' ')[0].strip()
        map_data = part.split(':')[1].strip().split('\n')
        maps[map_name] = []
        for row in map_data:
            maps[map_name].append(list(map(int, row.strip().split(' '))))


def get_lowest(value, map_name):
    for map_row in maps[map_name]:
        if map_row[1] <= value and value < (map_row[1] + map_row[2]):
            return map_row[0] + (value - map_row[1])
    return value

for seed in seeds:
    s2s = get_lowest(seed, 'seed-to-soil')
    s2f = get_lowest(s2s, 'soil-to-fertilizer')
    f2w = get_lowest(s2f, 'fertilizer-to-water')
    w2l = get_lowest(f2w, 'water-to-light')
    l2t = get_lowest(w2l, 'light-to-temperature')
    t2h = get_lowest(l2t, 'temperature-to-humidity')
    h2l = get_lowest(t2h, 'humidity-to-location')
    min_vals.append(h2l)

min_vals.sort()
min_val = min_vals.pop(0)
print(min_val)

# Part 2

seed_pairs = zip(seeds[::2], seeds[1::2])

def get_lowest_reverse(value, map_name):
    for map_row in maps[map_name]:
        if map_row[0] <= value and value < (map_row[0] + map_row[2]):
            return map_row[1] - (value - map_row[0])
    return value

def find_seed(value):
    for origin, size in seed_pairs:
        if origin <= value < origin + size:
            return True
    return False

for i in count():
    s2s = get_lowest_reverse(i, 'seed-to-soil')
    s2f = get_lowest_reverse(s2s, 'soil-to-fertilizer')
    f2w = get_lowest_reverse(s2f, 'fertilizer-to-water')
    w2l = get_lowest_reverse(f2w, 'water-to-light')
    l2t = get_lowest_reverse(w2l, 'light-to-temperature')
    t2h = get_lowest_reverse(l2t, 'temperature-to-humidity')
    h2l = get_lowest_reverse(t2h, 'humidity-to-location')
    print(i, h2l)
    if find_seed(h2l):
        print(i)
        break