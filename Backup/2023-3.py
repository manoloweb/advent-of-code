import os, re
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-3.txt')

pattern_symbols = r'[^\.\d]+'
pattern_digits = r'\d+'
line_count = 0
boundaries = {}

def get_set(matches):
    result = set()
    for match in matches:
        for i in range(match.start(), match.end()+2):
            result.add(i)
    return result

# Let's first map the boundaries of the symbols
with open(input_file_path, 'r') as input_file:
    for line in input_file:
        line_count += 1
        if line.strip() == '':
            continue
        matches = re.finditer(pattern_symbols, line.strip())
        the_set = get_set(matches)
        boundaries[line_count] = the_set if boundaries.get(line_count) == None else boundaries[line_count].union(the_set)
        boundaries[line_count-1] = the_set if boundaries.get(line_count-1) == None else boundaries[line_count-1].union(the_set)
        boundaries[line_count+1] = the_set if boundaries.get(line_count+1) == None else boundaries[line_count+1].union(the_set)
    del boundaries[0]

# Now let's find the digits and sum if within boundaries
result = 0
line_count = 0
debug = open('debug.txt', 'w')
with open(input_file_path, 'r') as input_file:
    for line in input_file:
        line_count += 1
        if line.strip() == '':
            continue
        matches = re.finditer(pattern_digits, line.strip())
        for match in matches:
            if (match.start()+1) in boundaries[line_count] or match.end() in boundaries[line_count]:
                result += int(match.group())
            
debug.close()        
print(result)


# Part 2 target: 80694070

with open(input_file_path, 'r') as input_file:
    lines = input_file.readlines()
    