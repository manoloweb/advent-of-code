import os
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2022-3.txt')

lowercase_modifier = 96 # Substract 96 so a = 1
uppercase_modifier = 38 # Substract 38 so A = 1

def get_letter_value(letter):
    ascii_value = ord(letter)
    return ascii_value - lowercase_modifier if ascii_value >= 97 else ascii_value - uppercase_modifier

def split_rucksack(rucksack):
    mid = len(rucksack) // 2
    return rucksack[:mid], rucksack[mid:]

def find_common(comp_1, comp_2):
    for letter in comp_1:
        if letter in comp_2:
            return letter
    return None

priorities_sum = 0

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        if line.strip() == '':
            continue
        rucksack = line.strip()
        rucksack_1, rucksack_2 = split_rucksack(rucksack)
        common_letter = find_common(rucksack_1, rucksack_2)
        priorities_sum += get_letter_value(common_letter)

print(priorities_sum)

# Second part

def find_common_badge(rs1, rs2, rs3):
    for letter in rs1:
        if letter in rs2 and letter in rs3:
            return letter
    return None

priorities_sum = 0

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        rucksack_1 = line.strip()
        rucksack_2 = input_file.readline().strip()
        rucksack_3 = input_file.readline().strip()
        common_letter = find_common_badge(rucksack_1, rucksack_2, rucksack_3)
        priorities_sum += get_letter_value(common_letter)


print(priorities_sum)