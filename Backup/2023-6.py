import os, re, operator
from functools import reduce
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-6.txt')

with open(input_file_path, 'r') as input_file:
    lines = input_file.readlines()
    parts = []
    for line in lines:
        parts.append(re.findall(r'(\d+)', line.strip()))
    plays = list(zip(parts[0], parts[1]))

def travel_win(secs, record):
    speed = 0
    wins = 0
    for i in range(1, secs):
        speed = i
        remaining_time = secs - i
        result = speed * remaining_time
        if result > record:
            wins += 1
    return wins

wins_count = []

for play in plays:
    wins_count.append(travel_win(int(play[0]), int(play[1])))

print(reduce(operator.mul, wins_count, 1))

# Part 2

def reduce_plays(plays):
    secs = ''
    record = ''
    for play in plays:
        secs += str(play[0])
        record += str(play[1])
    return (int(secs), int(record))

new_play = reduce_plays(plays)

print(travel_win(new_play[0], new_play[1]))