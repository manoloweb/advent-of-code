import os, re
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-11.txt')

empty_rows = []
empty_cols = []


with open(input_file_path, 'r') as file:
    universe = file.read()


galaxy_pattern = re.compile(r'([#])')
empty_rows = list(range(universe.count('\n')))
empty_cols = list(range(len(universe.splitlines()[0])))

def rem_element(type,element):
    global empty_rows, empty_cols
    if type == 'row':
        if element in empty_rows:
            empty_rows.remove(element)
    elif type == 'col':
        if element in empty_cols:
            empty_cols.remove(element)

row_count = 0
for row in universe.splitlines():
    if row.find('#') > -1:
        rem_element('row', row_count)
        found_galaxies = galaxy_pattern.finditer(row)
        for galaxy in found_galaxies:
            rem_element('col', galaxy.start())
    row_count += 1

universe = [list(row) for row in universe.splitlines()]

def expand_universe():
    global universe, empty_rows, empty_cols
    ec_count = 0
    for ec in empty_cols:
        for row in universe:
            row.insert(ec + ec_count, '.')
        ec_count += 1
    er_count = 0
    for er in empty_rows:
        universe.insert(er +er_count, ['.'] * len(universe[0]))
        er_count += 1

expand_universe()

all_galaxies = []


cur_row = 0
for row in universe:
    cur_col = 0
    for col in row:
        if col == '#':
            all_galaxies.append((cur_row, cur_col))
        cur_col += 1
    cur_row += 1


def galaxy_distance(galaxy1, galaxy2):
    return abs(galaxy1[0] - galaxy2[0]) + abs(galaxy1[1] - galaxy2[1])

cur_galaxy = 0
galaxy_distances = 0

for galaxy in all_galaxies:
    cur_galaxy += 1
    for i in range(cur_galaxy, len(all_galaxies)):
        galaxy_distances += galaxy_distance(galaxy, all_galaxies[i])

print(galaxy_distances)