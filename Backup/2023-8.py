import os, math
from itertools import cycle
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-8.txt')

maps = {}
line_num = 0
sides = []

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        # if first line
        if line_num == 0 or line.strip() == '':
            line_num += 1
            if line.strip() == '':
                continue
            else:
                #replace L with 0, and R with 1
                sides = list(map(lambda x: 0 if x == 'L' else 1, line.strip()))
            continue
        else:
            map_parts = line.strip().split(' = ')
            map_name = map_parts[0].strip()
            tup_vals = map_parts[1][1:-1].split(', ')
            map_vals = tuple(tup_vals)
            maps[map_name] = map_vals

next_position = 'AAA'
step_iterator = cycle(sides)
steps = 0
while True:
    next_selection = next(step_iterator)
    next_position = maps[next_position][next_selection]
    if next_position == 'ZZZ':
        break
    steps += 1


print(steps + 1)

# Part 2

start_group = [key for key in maps if key.endswith('A')]
steps_group = {}

for start in start_group:
    next_position = start
    step_iterator = cycle(sides)
    steps = 0
    while True:
        next_selection = next(step_iterator)
        next_position = maps[next_position][next_selection]
        if next_position.endswith('Z'):
            steps += 1
            steps_group[start] = steps
            break
        steps += 1

lcm_steps = math.lcm(*steps_group.values())

print(steps_group.values())
print(lcm_steps)