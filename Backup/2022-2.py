import os
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2022-2.txt')

points_table = {
    'A' : {'X' : 1 + 3, 'Y' : 2 + 6, 'Z' : 3 + 0},
    'B' : {'X' : 1 + 0, 'Y' : 2 + 3, 'Z' : 3 + 6},
    'C' : {'X' : 1 + 6, 'Y' : 2 + 0, 'Z' : 3 + 3}
}

total_points = 0

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        if line.strip() == '':
            continue
        line = line.strip().split(' ')
        total_points += points_table[line[0]][line[1]]

print(total_points)

# Second part

def translate_play(opponent_choice, expected_outcome):
    mappings = {
        'A': {'X': 'Z', 'Y': 'X', 'Z': 'Y'},
        'B': {'X': 'X', 'Y': 'Y', 'Z': 'Z'},
        'C': {'X': 'Y', 'Y': 'Z', 'Z': 'X'}
    }

    return mappings.get(opponent_choice).get(expected_outcome)

total_points = 0

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        if line.strip() == '':
            continue
        line = line.strip().split(' ')
        total_points += points_table[line[0]][translate_play(line[0], line[1])]

print(total_points)
