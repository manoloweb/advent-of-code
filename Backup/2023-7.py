import os

input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-7.txt')

card_map = {
    'A': 14,
    'K': 13,
    'Q': 12,
    'J': 11,
    'T': 10,
    '9': 9,
    '8': 8,
    '7': 7,
    '6': 6,
    '5': 5,
    '4': 4,
    '3': 3,
    '2': 2
}

deck = []
points = []
result = []

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        cards, points_num = line.strip().split(' ')
        points.append(int(points_num))
        card_tuple = ()
        for card in cards:
            card_tuple += (card_map[card],)
        deck.append(card_tuple)

def get_deck_value(deck):
    item_count = {}
    for item in deck:
        count = deck.count(item)
        item_count[item] = count
    res = sorted(item_count.items(), key=lambda x: x[1], reverse=True)
    if res[0][1] == 1:
        # Carta
        return 1
    elif res[0][1] == 2:
        if res[1][1] == 2:
            # 2 Pares
            return 3
        else:
            # Par
            return 2
    elif res[0][1] == 3:
        if res[1][1] == 2:
            # Full house
            return 5
        else:
            # Triple
            return 4
    elif res[0][1] == 4:
        # Pokar
        return 6
    elif res[0][1] == 5:
        # Quintilla
        return 7

for play in deck:
    result.append(get_deck_value(play))

ranked = tuple(zip(deck, result, points))

def rank_tuple(tpl):
    return (tpl[1], tuple(tpl[0]))

ranked = sorted(ranked, key=rank_tuple, reverse=False)

winnings = 0
count = 1
for play in ranked:
    winnings += play[2] * count
    count += 1

print(winnings)

# Part 2


card_map_joker = {
    'A': 13,
    'K': 12,
    'Q': 11,
    'J': 1,
    'T': 10,
    '9': 9,
    '8': 8,
    '7': 7,
    '6': 6,
    '5': 5,
    '4': 4,
    '3': 3,
    '2': 2
}

deck = []
points = []
result_joker = []

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        cards, points_num = line.strip().split(' ')
        points.append(int(points_num))
        card_tuple = ()
        for card in cards:
            card_tuple += (card_map_joker[card],)
        deck.append(card_tuple)


def get_deck_value_joker(deck):
    item_count = {}
    joker_count = 0
    for item in deck:
        count = deck.count(item)
        if item == 1:
            joker_count += 1
        else:
            item_count[item] = count
        item_count[item] = count
    res = sorted(item_count.items(), key=lambda x: x[1], reverse=True)
    highest = res[0][1] + joker_count
    if highest == 1:
        # Carta
        return 1
    elif highest == 2:
        if res[1][1] == 2:
            # 2 Pares
            return 3
        else:
            # Par
            return 2
    elif highest == 3:
        if res[1][1] == 2:
            # Full house
            return 5
        else:
            # Triple
            return 4
    elif highest == 4:
        # Pokar
        return 6
    elif highest == 5:
        # Quintilla
        return 7
    else:
        # Jokers first
        if res[0][0] == 1:
            if res[0][1] == 5:
                # Quintilla
                return 7
            else:
                sum_cards = res[0][1] + res[1][1]
                if sum_cards == 5:
                    return 7
                elif sum_cards == 4:
                    return 6
                elif sum_cards == 3:
                    return 4
                elif sum_cards == 2:
                    return 2
                else:
                    return 0
                



for play in deck:
    result_joker.append(get_deck_value_joker(play))

ranked_joker = tuple(zip(deck, result_joker, points))
ranked_joker = sorted(ranked_joker, key=rank_tuple, reverse=False)

winnings_joker = 0
count = 1

for play in ranked_joker:
    winnings_joker += play[2] * count
    print(play[0], play[1], play[2], count)
    count += 1

print(winnings_joker)