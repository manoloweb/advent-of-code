import os
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2022-6.txt')

def all_unique(chunk):
    return len(chunk) == len(set(chunk))

with open(input_file_path, 'r') as input_file:
    transmission = input_file.read().strip()

transmission_len = len(transmission)

for step in range(transmission_len):
    if all_unique(transmission[step:step+4]):
        print(step+4)
        break

# Part 2

for step in range(transmission_len):
    if all_unique(transmission[step:step+14]):
        print(step+14)
        break