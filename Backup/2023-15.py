import os, re

input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-15.txt')

with open(input_file_path, 'r') as input_file:
    steps = input_file.read().split(',')

def get_box_num(step):
    step_val = 0
    for i in range(len(step)):
        temp_val = step_val + ord(step[i])
        temp_val = temp_val * 17
        temp_val = temp_val % 256
        step_val = temp_val
    return step_val

boxes = {key: [] for key in range(256)}

def remove_lens(box, label):
    global boxes
    boxes[box] = [item for item in boxes[box] if label not in item]

def add_lens(box, label, val):
    global boxes
    label_index = next((index for (index, d) in enumerate(boxes[box]) if d["label"] == label), False)
    if label_index:
        boxes[box][label_index]["val"] = val
    else:
        boxes[box].append({"label": label, "val": val})


def identify_lens(text):
    pattern_minus = r'^([a-z]{2,})-$'
    pattern_equals = r'^([a-z]{2,})=(\d+)$'

    match_minus = re.match(pattern_minus, text)
    match_equals = re.match(pattern_equals, text)

    if match_minus:
        label = match_minus.group(1)
        return {'result': 'remove', 'label': label}
    elif match_equals:
        label = match_equals.group(1)
        val = int(match_equals.group(2))
        return {'result': 'add', 'label': label, 'val': val}
    else:
        raise Exception('Invalid lens: ' + text)

part1 = 0
for step in steps:
    my_box = get_box_num(step)
    part1 += my_box
    action = identify_lens(step)
    if action['result'] == 'remove':
        remove_lens(my_box, action['label'])
    elif action['result'] == 'add':
        add_lens(my_box, action['label'], action['val'])

focal_strenght = 0
for box in boxes:
    if len(boxes[box]) > 0:
        return_val = 0
        box_val = 1 + box
        for k, v in enumerate(boxes[box]):
            return_val += box_val * (k + 1) * v['val']
        focal_strenght += return_val
        


print(focal_strenght)

    
print(part1)