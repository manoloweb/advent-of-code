import os

input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-13.txt')

with open(input_file_path, 'r') as input_file:
    data = input_file.read().split('\n\n')

patches = []
for block in data:
    patches.append([list(row) for row in block.split('\n')])


def find_mirror(patch):
    count = 0
    max = len(patch)
    min = 0
    possible_mirrors = []
    for row in patch:
        if count == max-1:
            break
        if row == patch[count+1]:
            possible_mirrors.append(tuple((count, count+1)))
        count += 1
    if len(possible_mirrors) == 0:
        return None
    else:
        for pm in possible_mirrors:
            if pm[0] == 0 or pm[1] == max-1:
                return pm[0] + 1
            else:
                stretch = 0
                broke_mirror = False
                while pm[0]-stretch >= min and pm[1]+stretch <= max-1:
                    if patch[pm[0]-stretch] == patch[pm[1]+stretch]:
                        stretch += 1
                    else:
                        broke_mirror = True
                        break
                if stretch > 0 and not broke_mirror:
                    return pm[0] + 1
                else:
                    continue
        return None

def transpose(patch):
    return [list(row) for row in zip(*patch)]
vertical_patches = 0
horizontal_patches = 0
current_patch = 0
for patch in patches:
    is_mirror = find_mirror(patch)
    if is_mirror:
        vertical_patches += (100 * is_mirror)
        print('Found vertical mirror in patch ' + str(current_patch) + ' at row ' + str(is_mirror))
    else:
        is_mirror = find_mirror(transpose(patch))
        if is_mirror:
            horizontal_patches += is_mirror
            print('Found horizontal mirror in patch ' + str(current_patch) + ' at column ' + str(is_mirror))
        else:
            continue
    current_patch += 1
print('Vertical patches: ' + str(vertical_patches))
print('Horizontal patches: ' + str(horizontal_patches))
print('Total patches: ' + str(vertical_patches + horizontal_patches))