import os
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2022-1.txt')

elf = []
elf_count = 0

elf.insert(elf_count, 0)

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        if line.strip() == '':
            elf_count += 1
            elf.insert(elf_count, 0)
            continue
        elf[elf_count] += int(line.strip())

elf.sort(reverse=True)


print(elf[0])

# Second part

print(sum(elf[:3]))