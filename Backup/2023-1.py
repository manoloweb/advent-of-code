import os, re
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-1.txt')

def get_digits(value):
    digits = re.findall(r'\d', value)
    result = ''
    if len(digits) == 1:
        result = digits[0] + digits[0]
    else :
        result = digits[0] + digits[-1]
    return int(result)

calibration = 0

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        if line.strip() == '':
            continue
        calibration += get_digits(line.strip())

print(calibration)

# Part 2

def word_digits(value):
    # YES!! I did it the ugly way, so what!?
    replacements = {
        'oneight': '18',
        'threeight': '38',
        'fiveight': '58',
        'nineight': '98',
        'eightwo': '82',
        'eighthree': '83',
        'sevenine': '79',
        'twone': '21',
        'one': '1',
        'two': '2',
        'three': '3',
        'four': '4',
        'five': '5',
        'six': '6',
        'seven': '7',
        'eight': '8',
        'nine': '9'
    }
    for key, replacement in replacements.items():
        value = value.replace(key, replacement)
    return value

calibration = 0

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        if line.strip() == '':
            continue
        calibration += get_digits(word_digits(line.strip()))

print(calibration)