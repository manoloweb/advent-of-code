import os, re
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-4.txt')

num_pattern = r'\d+'

def make_set (value):
    nums = re.findall(num_pattern, value)
    result = set()
    for i in range(0, len(nums)):
        result.add(nums[i])
    return result

def get_value(value):
    if value == 0:
        return 0
    elif value == 1:
        return 1
    else:
        return 2 ** (value-1)

total_points = 0

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        if line.strip() == '':
            continue
        winners, numbers = line.strip().split('|')
        winners = winners.split(':')[1]
        winners = make_set(winners)
        numbers = make_set(numbers)
        total_points += (get_value(len(winners.intersection(numbers))))

print(total_points)

# Part 2

current_card = 0
card_copies = {}

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        current_card += 1
        if line.strip() == '':
            continue
        winners, numbers = line.strip().split('|')
        winners = winners.split(':')[1]
        winners = make_set(winners)
        numbers = make_set(numbers)
        matches = len(winners.intersection(numbers))
        card_copies[current_card] = card_copies[current_card] + 1 if card_copies.get(current_card) != None else 1
        for y in range(0, card_copies[current_card]):
            for i in range(1, matches+1):
                card_copies[current_card + i] = card_copies[current_card + i] + 1 if card_copies.get(current_card + i) != None else 1
print(sum(card_copies.values()))