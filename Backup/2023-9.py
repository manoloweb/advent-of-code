import os
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-9.txt')

def get_next(nums):
    res = []
    for i in range(len(nums)):
        if i == len(nums) - 1:
            continue
        else:
            res.append(-(nums[i] - nums[i + 1]))
    return res

sum_results = 0

with open(input_file_path, 'r') as file:
    for line in file:
        vals = line.strip()
        nums = list(map(int, vals.strip().split(' ')))
        steps = []
        while sum(nums) != 0:
            steps.append(nums)
            nums = get_next(nums)
        steps.reverse()
        result = 0
        for i in range(len(steps)):
            print(result, steps[i])
            result += steps[i][-1]
        sum_results += result
print(sum_results)