import os, math, re
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2022-5.txt')

section = 'header'
first = True
platform = {}
cols = 0
crane = 9001 # 9001 can move all containers in one move, 9000 can move one container at a time

with open(input_file_path, 'r') as input_file:
    for line in input_file:
        row = line.rstrip()
        if section == 'header':
            if first:
                first = False
                cols = math.ceil(len(row)/4)
                for i in range(cols+1):
                    platform[i+1] = []
            pos = 1
            col = 1
            while pos < len(row):
                if row[pos] == '1':
                    break
                if row[pos] != ' ':
                    platform[col].insert(0,row[pos])
                pos += 4
                col += 1
        elif section == 'body':
            nums = re.findall(r'\d+', row)
            times, origin, destination = map(int, nums)
            if crane == 9000:
                for i in range(times):
                    platform[destination].append(platform[origin].pop())
            else:
                platform[destination].extend(platform[origin][-times:])
                platform[origin] = platform[origin][:-times]
        if line.strip() == '':
            section = 'body'
            continue

top_row = ''

for cols in platform:
    top_row += platform[cols].pop()

print(top_row)