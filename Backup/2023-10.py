import os, math
input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2023-10.txt')

with open(input_file_path, 'r') as file:
    grid = []
    for line in file:
        grid.append(list(line.strip()))

def find_animal(grid):
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if grid[i][j] == 'S':
                return (i, j)


# Directions
R = (0,1)
L = (0,-1)
U = (-1,0)
D = (1,0)

#Possible flow characters
FR = '-J7'
FL = '-LF'
FU = '|F7'
FD = '|JL'

cur_pos = find_animal(grid)
steps = 0
direction_input = input('Animal found at: {}, which direction you want to try? (u,d,l,r)'.format(cur_pos)) #invalid? then assume Right
direction = D if direction_input == 'd' else U if direction_input == 'u' else L if direction_input == 'l' else R

boundaries = (0, len(grid), 0, len(grid[0]))

def move():
    global direction, cur_pos, steps, grid, boundaries, R, L, U, D
    target_pos = tuple(map(sum, zip(cur_pos, direction)))
    pipe_at = grid[target_pos[0]][target_pos[1]]
    if target_pos[0] < boundaries[0] or target_pos[0] >= boundaries[1] or target_pos[1] < boundaries[2] or target_pos[1] >= boundaries[3]:
        print('Out of bounds! at {}'.format(target_pos))
        return False
    else:
        if direction == R and pipe_at in FR:
            cur_pos = target_pos
        elif direction == L and pipe_at in FL:
            cur_pos = target_pos
        elif direction == U and pipe_at in FU:
            cur_pos = target_pos
        elif direction == D and pipe_at in FD:
            cur_pos = target_pos
        elif pipe_at == 'S':
            print('Found the animal! at {}'.format(target_pos))
            return False
        else:
            print('Invalid move! at {}'.format(target_pos))
            print('Pipe at {} is not compatible with direction {}'.format(pipe_at, direction))
            return False
        # Let's figure out where should we go now
        if direction == R:
            direction = U if pipe_at == 'J' else D if pipe_at == '7' else R
        elif direction == L:
            direction = U if pipe_at == 'L' else D if pipe_at == 'F' else L
        elif direction == U:
            direction = R if pipe_at == 'F' else L if pipe_at == '7' else U
        elif direction == D:
            direction = L if pipe_at == 'J' else R if pipe_at == 'L' else D

        print('Moved to {}, found pipe {}'.format(cur_pos, pipe_at))
        steps += 1
        return True

while move():
    pass

print(math.ceil(steps/2))
