import os, re

input_file_path = os.path.join(os.path.dirname(__file__), 'inputs', '2024-01.txt')
sum_a = []
sum_b = []
total_diff = 0
for line in open(input_file_path):
    match = re.match(r'(\d+)\s+(\d+)', line)
    if match:
        a, b = map(int, match.groups())
        sum_a.insert(0, a)
        sum_b.insert(0, b)
    else:
        print('No match:', line)

sum_a.sort()
sum_b.sort()

for i in range(len(sum_a)):
    total_diff += abs(sum_b[i] - sum_a[i])

print(total_diff)